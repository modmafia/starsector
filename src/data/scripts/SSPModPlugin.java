package data.scripts;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignEventListener;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.impl.campaign.events.CoreEventProbabilityManager;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.shared.SharedData;
import com.thoughtworks.xstream.XStream;
import data.scripts.campaign.SSP_CampaignPlugin;
import data.scripts.campaign.events.SSP_CrewHandlerEvent;
import data.scripts.campaign.events.SSP_EventProbabilityManager;
import data.scripts.campaign.events.SSP_FactionVengeanceEvent;
import data.scripts.campaign.events.SSP_FactionVengeanceEvent.VengeanceDef;
import data.scripts.world.SSP_AsteroidTracker;
import java.io.IOException;
import org.apache.log4j.Level;
import org.json.JSONException;
import org.json.JSONObject;

public class SSPModPlugin extends BaseModPlugin {

    public static boolean Module_CrewSalaries = true;
    public static boolean Module_FactionVengeance = true;
    public static boolean Module_OfficerDeath = true;

    public static float Param_CrewSalaries = 1.0f;
    public static boolean Param_OfficerDaredevilBonus = true;
    public static boolean SHOW_DEBUG_INFO = false;

    public static boolean blackrockExists = false;
    public static boolean citadelExists = false;
    public static boolean diableExists = false;
    public static boolean exigencyExists = false;
    public static boolean hasGraphicsLib = false;
    public static boolean hasSWP = false;
    public static boolean hasStarsectorPlus = false;
    public static boolean hasTwigLib = false;
    public static boolean hasUnderworld = false;
    public static boolean imperiumExists = false;
    public static boolean isExerelin = false;
    public static boolean junkPiratesExists = false;
    public static boolean mayorateExists = false;
    public static boolean oraExists = false;
    public static boolean scyExists = false;
    public static boolean shadowyardsExists = false;
    public static boolean templarsExists = false;
    public static boolean tiandongExists = false;

    private static final String SETTINGS_FILE = "SSP_OPTIONS.ini";

    public static void removeScriptAndListener(Class<?> oldClass, Class<?> newClass) {
        Global.getSector().removeScriptsOfClass(oldClass);
        CampaignEventListener listener = null;
        for (CampaignEventListener l : Global.getSector().getAllListeners()) {
            if (oldClass.getClass().isInstance(l) && !newClass.getClass().isInstance(l)) {
                listener = l;
                break;
            }
        }
        if (listener != null) {
            Global.getSector().removeListener(listener);
        }
    }

    public static void syncSSPScripts() {
        if (!Global.getSector().hasScript(SSP_EventProbabilityManager.class)) {
            removeScriptAndListener(CoreEventProbabilityManager.class, SSP_EventProbabilityManager.class);
            SSP_EventProbabilityManager probabilityManager = new SSP_EventProbabilityManager();
            Global.getSector().getPersistentData().put("ssp_eventProbabilityManager", probabilityManager);
            Global.getSector().addScript(probabilityManager);
        }
    }

    public static void syncSSPScriptsExerelin() {
        if (!Global.getSector().hasScript(SSP_EventProbabilityManager.class)) {
            removeScriptAndListener(CoreEventProbabilityManager.class, SSP_EventProbabilityManager.class);
            SSP_EventProbabilityManager probabilityManager = new SSP_EventProbabilityManager();
            Global.getSector().getPersistentData().put("ssp_eventProbabilityManager", probabilityManager);
            Global.getSector().addScript(probabilityManager);
        }
    }

    private static void loadSettings() throws IOException, JSONException {
        JSONObject settings = Global.getSettings().loadJSON(SETTINGS_FILE);

        Param_CrewSalaries = (float) settings.getDouble("crewSalaries");
        Param_OfficerDaredevilBonus = settings.getBoolean("officerDaredevilBonus");

        Module_CrewSalaries = Param_CrewSalaries > 0f;
        Module_FactionVengeance = settings.getBoolean("factionVengeance");
        Module_OfficerDeath = settings.getBoolean("officerDeath");
    }

    @Override
    public void configureXStream(XStream x) {
        x.alias("SSP_AstT", SSP_AsteroidTracker.class);
        x.alias("SSP_CrwHndE", SSP_CrewHandlerEvent.class);
        x.alias("SSP_EvtPM", SSP_EventProbabilityManager.class);
        x.alias("SSP_CPgn", SSP_CampaignPlugin.class);
        x.alias("SSP_VngE", SSP_FactionVengeanceEvent.class);
        x.alias("SSP_VngD", VengeanceDef.class);
    }

    @Override
    public void onApplicationLoad() throws Exception {
        boolean hasLazyLib = Global.getSettings().getModManager().isModEnabled("lw_lazylib");
        if (!hasLazyLib) {
            throw new RuntimeException("Starsector+ requires LazyLib!");
        }
        hasGraphicsLib = Global.getSettings().getModManager().isModEnabled("shaderLib");
        if (!hasGraphicsLib) {
            throw new RuntimeException("Starsector+ requires GraphicsLib!");
        } else {
            SSP_ModPluginAlt.initGraphicsLib();
        }

        isExerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
        hasTwigLib = Global.getSettings().getModManager().isModEnabled("ztwiglib");
        hasSWP = Global.getSettings().getModManager().isModEnabled("swp");
        hasUnderworld = Global.getSettings().getModManager().isModEnabled("underworld");

        try {
            loadSettings();
        } catch (IOException | JSONException e) {
            Global.getLogger(SSPModPlugin.class).log(Level.ERROR, "Settings loading failed! " + e.getMessage());
        }

        imperiumExists = Global.getSettings().getModManager().isModEnabled("Imperium");
        templarsExists = Global.getSettings().getModManager().isModEnabled("Templars");
        blackrockExists = Global.getSettings().getModManager().isModEnabled("blackrock_driveyards");
        exigencyExists = Global.getSettings().getModManager().isModEnabled("exigency");
        citadelExists = Global.getSettings().getModManager().isModEnabled("Citadel");
        shadowyardsExists = Global.getSettings().getModManager().isModEnabled("shadow_ships");
        mayorateExists = Global.getSettings().getModManager().isModEnabled("mayorate");
        junkPiratesExists = Global.getSettings().getModManager().isModEnabled("junk_pirates_release");
        scyExists = Global.getSettings().getModManager().isModEnabled("SCY");
        oraExists = Global.getSettings().getModManager().isModEnabled("ORA");
        tiandongExists = Global.getSettings().getModManager().isModEnabled("THI");
        diableExists = Global.getSettings().getModManager().isModEnabled("diableavionics");
    }

    @Override
    public void onGameLoad(boolean newGame) {
        if (!newGame) {
            Boolean inUse = (Boolean) Global.getSector().getPersistentData().get("SSP_in_use");
            if (inUse == null || inUse == false) {
                throw new RuntimeException("You cannot add SS+ to an existing campaign!");
            }
        }

        if (isExerelin) {
            syncSSPScriptsExerelin();
        } else {
            syncSSPScripts();
        }

        if (Module_CrewSalaries) {
            if (!Global.getSector().getEventManager().isOngoing(null, "crew_handler_event")) {
                Global.getSector().getEventManager().startEvent(null, "crew_handler_event", null);
            }
        }

        if (Global.getSector().getPersistentData().containsKey("SSP_initialized")) {
            Global.getSector().getPersistentData().remove("SSP_initialized");
        } else {
            Global.getSector().registerPlugin(new SSP_CampaignPlugin());
        }

        Global.getSector().addTransientScript(new SSP_AsteroidTracker());
    }

    @Override
    public void onNewGame() {
        Global.getSector().getPersistentData().put("SSP_in_use", true);
        Global.getSector().getPersistentData().put("SSP_initialized", true);
        Global.getSector().registerPlugin(new SSP_CampaignPlugin());

        // Higher chance to post bounties
        float otherFactions = 0;
        if (blackrockExists) {
            otherFactions += 1f;
        }
        if (citadelExists) {
            otherFactions += 1f;
        }
        if (exigencyExists) {
            otherFactions += 0.5f;
        }
        if (imperiumExists) {
            otherFactions += 1f;
        }
        if (junkPiratesExists) {
            otherFactions += 2.5f;
        }
        if (mayorateExists) {
            otherFactions += 0f; // Don't give bounties yet
        }
        if (shadowyardsExists) {
            otherFactions += 1f;
        }
        if (scyExists) {
            otherFactions += 1f;
        }
        if (tiandongExists) {
            otherFactions += 1f;
        }
        if (oraExists) {
            otherFactions += 1f;
        }
        if (otherFactions >= 3f) {
            SharedData.getData().getPersonBountyEventData().addParticipatingFaction(Factions.HEGEMONY);
        }
        if (otherFactions >= 5f) {
            SharedData.getData().getPersonBountyEventData().addParticipatingFaction(Factions.INDEPENDENT);
            SharedData.getData().getPersonBountyEventData().addParticipatingFaction(Factions.PERSEAN);
        }
        if (otherFactions >= 6f) {
            SharedData.getData().getPersonBountyEventData().addParticipatingFaction(Factions.HEGEMONY);
        }
        if (otherFactions >= 7f) {
            SharedData.getData().getPersonBountyEventData().addParticipatingFaction(Factions.TRITACHYON);
            SharedData.getData().getPersonBountyEventData().addParticipatingFaction(Factions.PERSEAN);
        }
        if (otherFactions >= 9f) {
            SharedData.getData().getPersonBountyEventData().addParticipatingFaction(Factions.HEGEMONY);
        }

        if (isExerelin) {
            syncSSPScriptsExerelin();
        } else {
            syncSSPScripts();
        }
    }

    @Override
    public void onNewGameAfterTimePass() {
        SectorAPI sector = Global.getSector();
        FactionAPI player = sector.getFaction(Factions.PLAYER);
        for (FactionAPI faction : Global.getSector().getAllFactions()) {
            if (faction != player) {
                if (!faction.getId().contentEquals(Factions.INDEPENDENT) && !faction.isNeutralFaction() &&
                        !faction.getId().contentEquals("merc_hostile")) {
                    faction.setRelationship("merc_hostile", faction.getRelationship(Factions.INDEPENDENT));
                }
            }
        }
    }
}
