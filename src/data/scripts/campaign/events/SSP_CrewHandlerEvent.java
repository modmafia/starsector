package data.scripts.campaign.events;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignClockAPI;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.comm.MessagePriority;
import com.fs.starfarer.api.campaign.events.CampaignEventTarget;
import com.fs.starfarer.api.characters.OfficerDataAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.events.BaseEventPlugin;
import com.fs.starfarer.api.impl.campaign.ids.Strings;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import data.scripts.SSPModPlugin;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SSP_CrewHandlerEvent extends BaseEventPlugin {

    private float debtTime = 0f;
    private boolean inDebt = false;
    private final IntervalUtil interval = new IntervalUtil(1f, 1f);
    private int month;
    private final IntervalUtil mutinyInterval = new IntervalUtil(3f, 5f);
    private float paidSinceLastMonth = 0f;
    private float unpaid = 0f;
    private float yearlyCrew = 0f;
    private float yearlyMarines = 0f;
    private float yearlyOfficers = 0f;

    @Override
    public void advance(float amount) {
        if (!SSPModPlugin.Module_CrewSalaries) {
            return;
        }

        CampaignFleetAPI player = Global.getSector().getPlayerFleet();
        if (player == null) {
            return;
        }

        if (Global.getSector().getClock().getMonth() != month) {
            month = Global.getSector().getClock().getMonth();
            if (inDebt) {
                Global.getSector().reportEventStage(this, "debt", Global.getSector().getPlayerFleet(),
                                                    MessagePriority.DELIVER_IMMEDIATELY);
            } else {
                Global.getSector().reportEventStage(this, "paid", Global.getSector().getPlayerFleet(),
                                                    MessagePriority.DELIVER_IMMEDIATELY);
            }
            Global.getSector().getPersistentData().put("salariesClock", Global.getSector().getClock().createClock(
                                                       Global.getSector().getClock().getTimestamp()));
            paidSinceLastMonth = 0f;
        }

        float days = Global.getSector().getClock().convertToDays(amount);
        interval.advance(days);
        if (interval.intervalElapsed()) {
            float credits, crewCredits = 0f, marineCredits = 0f, officerCredits = 0f;
            crewCredits += player.getCargo().getCrew() * 100f;
            marineCredits += player.getCargo().getMarines() * 200f;
            for (OfficerDataAPI data : player.getFleetData().getOfficersCopy()) {
                officerCredits += 2000f + 1000f * data.getPerson().getStats().getLevel();
            }
            yearlyCrew = crewCredits;
            yearlyMarines = marineCredits;
            yearlyOfficers = officerCredits;
            credits = crewCredits + marineCredits + officerCredits;
            credits *= SSPModPlugin.Param_CrewSalaries;
            credits /= 365f;
            credits += unpaid;
            float toPay = (float) Math.floor(credits);

            if (player.getCargo().getCredits().get() < toPay) {
                inDebt = true;
                toPay = player.getCargo().getCredits().get();
                int debt = (int) (credits - toPay);
                Global.getSector().getCampaignUI().addMessage("Cannot pay salaries! Debt: " + debt + Strings.C,
                                                              Global.getSettings().getColor(
                                                                      "textEnemyColor"), "" + debt,
                                                              Global.getSettings().getColor("buttonShortcut"));
            } else {
                inDebt = false;
                debtTime = 0f;
            }

            unpaid = credits - toPay;
            player.getCargo().getCredits().subtract(toPay);
            paidSinceLastMonth += toPay;
        }

        if (inDebt) {
            debtTime += days;
            mutinyInterval.advance(days);
            if (mutinyInterval.intervalElapsed()) {
                int worstDebt = 0;
                if (debtTime < 7f) {
                    List<FleetMemberAPI> members = player.getFleetData().getMembersListCopy();
                    for (FleetMemberAPI member : members) {
                        if (Math.random() > 0.5) {
                            worstDebt = 1;
                            member.getRepairTracker().applyCREvent(-0.1f, "Morale");
                        }
                    }
                } else if (debtTime < 14f) {
                    List<FleetMemberAPI> members = player.getFleetData().getMembersListCopy();
                    for (FleetMemberAPI member : members) {
                        if (Math.random() > 0.5) {
                            worstDebt = 2;
                            member.getRepairTracker().applyCREvent(-0.2f, "Strike");
                        } else if (Math.random() > 0.5) {
                            worstDebt = 1;
                            member.getRepairTracker().applyCREvent(-0.1f, "Morale");
                        }
                    }
                } else if (debtTime < 21f) {
                    List<FleetMemberAPI> members = player.getFleetData().getMembersListCopy();
                    for (FleetMemberAPI member : members) {
                        if (Math.random() > 0.5) {
                            worstDebt = 3;
                            member.getRepairTracker().applyCREvent(-0.3f, "Revolt");
                            member.getStatus().applyHullFractionDamage(0.1f);
                        } else if (Math.random() > 0.5) {
                            worstDebt = 2;
                            member.getRepairTracker().applyCREvent(-0.2f, "Strike");
                        } else if (Math.random() > 0.5) {
                            worstDebt = 1;
                            member.getRepairTracker().applyCREvent(-0.1f, "Morale");
                        }
                    }
                } else {
                    List<FleetMemberAPI> members = player.getFleetData().getMembersListCopy();
                    for (FleetMemberAPI member : members) {
                        if (Math.random() > 0.5) {
                            worstDebt = 4;
                            member.getRepairTracker().applyCREvent(-0.4f, "Mutiny");
                            member.getStatus().applyHullFractionDamage(0.2f);
                        } else if (Math.random() > 0.5) {
                            worstDebt = 3;
                            member.getRepairTracker().applyCREvent(-0.3f, "Revolt");
                            member.getStatus().applyHullFractionDamage(0.1f);
                        } else if (Math.random() > 0.5) {
                            worstDebt = 2;
                            member.getRepairTracker().applyCREvent(-0.2f, "Strike");
                        } else if (Math.random() > 0.5) {
                            worstDebt = 1;
                            member.getRepairTracker().applyCREvent(-0.1f, "Morale");
                        }
                    }
                }

                if (worstDebt == 1) {
                    Global.getSector().getCampaignUI().addMessage("Morale dropping due to unpaid salaries",
                                                                  Global.getSettings().getColor("textEnemyColor"));
                } else if (worstDebt == 2) {
                    Global.getSector().getCampaignUI().addMessage("Strikes due to unpaid salaries",
                                                                  Global.getSettings().getColor("textEnemyColor"));
                } else if (worstDebt == 3) {
                    Global.getSector().getCampaignUI().addMessage("Revolts due to unpaid salaries",
                                                                  Global.getSettings().getColor("textEnemyColor"));
                } else if (worstDebt == 4) {
                    Global.getSector().getCampaignUI().addMessage("Mutiny due to unpaid salaries",
                                                                  Global.getSettings().getColor("textEnemyColor"));
                }
            }
        }
    }

    @Override
    public CampaignEventCategory getEventCategory() {
        return CampaignEventCategory.DO_NOT_SHOW_IN_MESSAGE_FILTER;
    }

    @Override
    public String getEventName() {
        return "Salary Report";
    }

    @Override
    public Color[] getHighlightColors(String stageId) {
        Color[] colors = new Color[5];
        colors[0] = Misc.getHighlightColor();
        colors[1] = Misc.getHighlightColor();
        colors[2] = Misc.getHighlightColor();
        colors[3] = Misc.getHighlightColor();
        colors[4] = Misc.getNegativeHighlightColor();
        return colors;
    }

    @Override
    public String[] getHighlights(String stageId) {
        List<String> result = new ArrayList<>(5);
        addTokensToList(result, "$paid");
        addTokensToList(result, "$yearlyCrew");
        addTokensToList(result, "$yearlyMarines");
        addTokensToList(result, "$yearlyOfficers");
        addTokensToList(result, "$unpaid");
        return result.toArray(new String[result.size()]);
    }

    @Override
    public Map<String, String> getTokenReplacements() {
        Map<String, String> map = super.getTokenReplacements();
        CampaignClockAPI previous = (CampaignClockAPI) Global.getSector().getPersistentData().get("salariesClock");
        if (previous != null) {
            map.put("$date", previous.getMonthString() + ", c." + previous.getCycle());
        }
        map.put("$paid", Misc.getDGSCredits(paidSinceLastMonth));
        map.put("$yearlyCrew", Misc.getDGSCredits(yearlyCrew));
        map.put("$yearlyMarines", Misc.getDGSCredits(yearlyMarines));
        map.put("$yearlyOfficers", Misc.getDGSCredits(yearlyOfficers));
        map.put("$unpaid", Misc.getDGSCredits(unpaid));
        return map;
    }

    @Override
    public void init(String type, CampaignEventTarget eventTarget) {
        super.init(type, eventTarget, false);
        Global.getSector().getPersistentData().put("salariesClock", Global.getSector().getClock().createClock(
                                                   Global.getSector().getClock().getTimestamp()));
        month = Global.getSector().getClock().getMonth();
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public boolean showAllMessagesIfOngoing() {
        return false;
    }
}
