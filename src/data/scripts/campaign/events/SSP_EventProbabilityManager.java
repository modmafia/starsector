package data.scripts.campaign.events;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.events.EventProbabilityAPI;
import com.fs.starfarer.api.impl.campaign.events.CoreEventProbabilityManager;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.SSPModPlugin;
import data.scripts.campaign.events.SSP_FactionVengeanceEvent.VengeanceDef;
import data.scripts.util.SSP_Util;
import java.util.EnumMap;
import java.util.Map;
import org.apache.log4j.Logger;

// You don't want it to be final?  Well, screw you!
public final class SSP_EventProbabilityManager extends CoreEventProbabilityManager {

    @SuppressWarnings("FieldNameHidesFieldInSuperclass")
    public static Logger log = Global.getLogger(SSP_EventProbabilityManager.class);

    public transient Map<VengeanceDef, Float> escalation;

    private float escalationScale = 1f;
    private final IntervalUtil factionVengeanceInterval;

    public SSP_EventProbabilityManager() {
        super();

        factionVengeanceInterval = new IntervalUtil(5f, 10f);
        readResolve();
    }

    @Override
    public void advance(float amount) {
        super.advance(amount);

        float days = Global.getSector().getClock().convertToDays(amount);

        calculateFactionVengeance(days);
    }

    private void calculateFactionVengeance(float days) {
        if (!SSPModPlugin.Module_FactionVengeance) {
            return;
        }
        if (Global.getSector().isInNewGameAdvance()) {
            return;
        }

        factionVengeanceInterval.advance(days);
        if (factionVengeanceInterval.intervalElapsed()) {
            FactionAPI player = Global.getSector().getPlayerFaction();

            float totalVengeance = 1f;
            for (VengeanceDef def : VengeanceDef.values()) {
                if (Global.getSector().getFaction(def.faction) != null) {
                    totalVengeance += escalation.get(def) * (float) Math.sqrt(def.vengefulness);
                }
            }

            float powerScale = Math.min(1f, SSP_Util.calculatePowerLevel(Global.getSector().getPlayerFleet()) / 250f);

            float difficultyScale = powerScale * (float) Math.sqrt(2f / totalVengeance);
            log.info("Scaling faction vengeance by " + (difficultyScale * escalationScale));

            for (VengeanceDef def : VengeanceDef.values()) {
                FactionAPI faction = Global.getSector().getFaction(def.faction);
                if (faction == null) {
                    continue;
                }
                Float start = escalation.get(def);

                float level;
                float decayFactor = (float) Math.sqrt(def.vengefulness);
                if (faction.isAtBest(player, RepLevel.VENGEFUL)) {
                    float rel = faction.getRelationship(player.getId());
                    float scale = SSP_Util.lerp(0.25f, 1f, (rel + RepLevel.VENGEFUL.getMin()) *
                                                (-1f / Math.abs(RepLevel.VENGEFUL.getMin() - RepLevel.VENGEFUL.getMax())));
                    level = Math.min(5f, start + (2f / 52f) * scale * difficultyScale * def.vengefulness);
                    MarketAPI market = pickMarketForFactionVengeance(faction);
                    if (market != null) {
                        EventProbabilityAPI probability = Global.getSector().getEventManager().getProbability(
                                            "faction_vengeance", market);
                        probability.increaseProbability((float) Math.sqrt(def.vengefulness) * escalationScale * scale *
                                difficultyScale * difficultyScale * (2f / 52f));
                        log.info("Increasing " + market.getName() + " vengeance probability to " +
                                probability.getProbability());
                    }
                } else if (faction.isAtBest(player, RepLevel.HOSTILE)) {
                    level = Math.max(0f, start - (1f / 52f) * decayFactor);
                } else if (faction.isAtBest(player, RepLevel.INHOSPITABLE)) {
                    level = Math.max(0f, start - (2f / 52f) * decayFactor);
                } else if (faction.isAtBest(player, RepLevel.SUSPICIOUS)) {
                    level = Math.max(0f, start - (3f / 52f) * decayFactor);
                } else if (faction.isAtBest(player, RepLevel.NEUTRAL)) {
                    level = Math.max(0f, start - (4f / 52f) * decayFactor);
                } else if (faction.isAtBest(player, RepLevel.FAVORABLE)) {
                    level = Math.max(0f, Math.min(3f, start - (2f / 12f) * decayFactor));
                } else if (faction.isAtBest(player, RepLevel.WELCOMING)) {
                    level = Math.max(0f, Math.min(2f, start - (4f / 12f) * decayFactor));
                } else if (faction.isAtBest(player, RepLevel.FRIENDLY)) {
                    level = Math.max(0f, Math.min(1f, start - (6f / 12f) * decayFactor));
                } else {
                    level = 0f;
                }

                escalation.put(def, escalation.get(def) + (level - start));
                if (start < level) {
                    log.info("Increasing " + faction.getDisplayName() + " vengeance level to " + level);
                } else if (start > level) {
                    log.info("Decreasing " + faction.getDisplayName() + " vengeance level to " + level);
                }
            }
        }
    }

    private MarketAPI pickMarketForFactionVengeance(FactionAPI faction) {
        WeightedRandomPicker<MarketAPI> picker = new WeightedRandomPicker<>();
        float total = 0f;
        for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy()) {
            if (faction.getId().contentEquals("cabal")) {
                if (market.getFaction() != faction && !market.hasCondition("cabal_influence")) {
                    continue;
                }
            } else {
                if (market.getFaction() != faction) {
                    continue;
                }
            }
            float weight = market.getSize() * (float) Math.sqrt(SSP_Util.lerp(0.25f, 1f, market.getShipQualityFactor()));
            float mod = 1f;
            if (market.hasCondition(Conditions.MILITARY_BASE) || market.hasCondition("ii_interstellarbazaar")) {
                mod += 0.15f;
            }
            if (market.hasCondition(Conditions.HEADQUARTERS)) {
                mod += 0.1f;
            }
            if (market.hasCondition(Conditions.REGIONAL_CAPITAL)) {
                mod += 0.1f;
            }
            if (market.hasCondition(Conditions.SPACEPORT) || market.hasCondition(Conditions.ORBITAL_STATION)) {
                mod += 0.15f;
            }
            weight *= mod;
            total += weight;
        }
        for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy()) {
            if (faction.getId().contentEquals("cabal")) {
                if (market.getFaction() != faction && !market.hasCondition("cabal_influence")) {
                    continue;
                }
            } else {
                if (market.getFaction() != faction) {
                    continue;
                }
            }
            float weight = market.getSize() * (float) Math.sqrt(SSP_Util.lerp(0.25f, 1f, market.getShipQualityFactor()));
            float mod = 1f;
            if (market.hasCondition(Conditions.MILITARY_BASE) || market.hasCondition("ii_interstellarbazaar")) {
                mod += 0.15f;
            }
            if (market.hasCondition(Conditions.HEADQUARTERS)) {
                mod += 0.1f;
            }
            if (market.hasCondition(Conditions.REGIONAL_CAPITAL)) {
                mod += 0.1f;
            }
            if (market.hasCondition(Conditions.SPACEPORT) || market.hasCondition(Conditions.ORBITAL_STATION)) {
                mod += 0.15f;
            }
            weight *= mod;

            EventProbabilityAPI probability = Global.getSector().getEventManager().getProbability("faction_vengeance",
                                                                                                  market);
            if (probability.getProbability() > 0f) {
                weight *= total * (1f + probability.getProbability());
            }
            picker.add(market, weight);
        }
        return picker.pick();
    }

    @Override
    protected Object readResolve() {
        super.readResolve();
        escalation = new EnumMap<>(VengeanceDef.class);
        float escalationAmt = 0;
        for (VengeanceDef def : VengeanceDef.values()) {
            if (Global.getSector().getFaction(def.faction) != null) {
                escalationAmt += def.vengefulness;
                escalation.put(def, 0f);
            }
        }
        escalationScale = (float) Math.pow(4.0 / escalationAmt, 0.75);
        return this;
    }
}
