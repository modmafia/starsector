package data.scripts.campaign;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.FleetDataAPI;
import com.fs.starfarer.api.characters.OfficerDataAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.events.OfficerManagerEvent;
import com.fs.starfarer.api.impl.campaign.events.OfficerManagerEvent.SkillPickPreference;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2;
import com.fs.starfarer.api.loading.FleetCompositionDoctrineAPI;
import com.fs.starfarer.api.plugins.OfficerLevelupPlugin;
import java.util.Random;

public class SSP_FleetFactory extends FleetFactoryV2 {

    public static CampaignFleetAPI enhancedCreateFleet(FactionAPI faction, int fleetSize, FleetFactoryDelegate delegate) {
        FleetCompositionDoctrineAPI doctrine = faction.getCompositionDoctrine();
        float preSmall = doctrine.getSmall();
        float preFast = doctrine.getFast();
        float preMedium = doctrine.getMedium();
        float preLarge = doctrine.getLarge();
        float preCapital = doctrine.getCapital();
        float preSmallCarrierProbability = doctrine.getSmallCarrierProbability();
        float preMediumCarrierProbability = doctrine.getMediumCarrierProbability();
        float preLargeCarrierProbability = doctrine.getLargeCarrierProbability();

        if (fleetSize > 25 && fleetSize <= 50) {
            doctrine.setSmall(preSmall * 0.5f);
            doctrine.setFast(preFast * 0.5f);
            doctrine.setMedium(preMedium);
            doctrine.setLarge(preLarge * 1.25f);
            doctrine.setCapital(preCapital * 1.5f);
            doctrine.setSmallCarrierProbability(preSmallCarrierProbability * 0.8f);
            doctrine.setMediumCarrierProbability(preMediumCarrierProbability * 0.9f);
            doctrine.setLargeCarrierProbability(preLargeCarrierProbability);
        } else if (fleetSize > 50 && fleetSize <= 100) {
            doctrine.setSmall(preSmall * 0.25f);
            doctrine.setFast(preFast * 0.25f);
            doctrine.setMedium(preMedium * 0.75f);
            doctrine.setLarge(preLarge);
            doctrine.setCapital(preCapital * 1.25f);
            doctrine.setSmallCarrierProbability(preSmallCarrierProbability * 0.5f);
            doctrine.setMediumCarrierProbability(preMediumCarrierProbability * 0.65f);
            doctrine.setLargeCarrierProbability(preLargeCarrierProbability * 0.8f);
        } else if (fleetSize > 100) {
            doctrine.setSmall(preSmall * 0.125f);
            doctrine.setFast(preFast * 0.125f);
            doctrine.setMedium(preMedium * 0.375f);
            doctrine.setLarge(preLarge * 0.75f);
            doctrine.setCapital(preCapital);
            doctrine.setSmallCarrierProbability(preSmallCarrierProbability * 0.2f);
            doctrine.setMediumCarrierProbability(preMediumCarrierProbability * 0.4f);
            doctrine.setLargeCarrierProbability(preLargeCarrierProbability * 0.6f);
        }

        CampaignFleetAPI fleet = delegate.createFleet();

        doctrine.setSmall(preSmall);
        doctrine.setFast(preFast);
        doctrine.setMedium(preMedium);
        doctrine.setLarge(preLarge);
        doctrine.setCapital(preCapital);
        doctrine.setSmallCarrierProbability(preSmallCarrierProbability);
        doctrine.setMediumCarrierProbability(preMediumCarrierProbability);
        doctrine.setLargeCarrierProbability(preLargeCarrierProbability);

        return fleet;
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static void levelOfficer(PersonAPI person, FleetDataAPI fleetData, int toLevel,
                                    boolean alwaysPickHigherSkill, SkillPickPreference pref, Random random) {
        if (random == null) {
            random = new Random();
        }

        OfficerLevelupPlugin plugin = (OfficerLevelupPlugin) Global.getSettings().getPlugin("officerLevelUp");
        person.getStats().setSkipRefresh(true);

        OfficerDataAPI officerData = fleetData.getOfficerData(person);
        if (officerData == null) {
            return;
        }

        long xp = plugin.getXPForLevel(toLevel);
        officerData.addXP(xp - person.getStats().getXP());
        while (officerData.canLevelUp()) {
            String skillId =
                   OfficerManagerEvent.pickSkill(officerData.getPerson(), officerData.getSkillPicks(),
                                                 alwaysPickHigherSkill, pref, random);
            if (skillId != null) {
                officerData.levelUp(skillId);
            }
        }

        person.getStats().setSkipRefresh(false);
        person.getStats().refreshCharacterStatsEffects();
    }

    public interface FleetFactoryDelegate {

        public CampaignFleetAPI createFleet();
    }
}
