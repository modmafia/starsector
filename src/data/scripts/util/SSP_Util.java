package data.scripts.util;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FleetDataAPI;
import com.fs.starfarer.api.campaign.econ.CommodityOnMarketAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.characters.OfficerDataAPI;
import com.fs.starfarer.api.combat.ShipHullSpecAPI.ShipTypeHints;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import data.scripts.SSPModPlugin;
import java.awt.Color;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SSP_Util {

    public static final Set<String> SPECIAL_SHIPS = new HashSet<>(44);

    static {
        SPECIAL_SHIPS.add("ii_boss_praetorian");
        SPECIAL_SHIPS.add("ii_boss_olympus");
        SPECIAL_SHIPS.add("ii_boss_dominus");
        SPECIAL_SHIPS.add("msp_boss_potniaBis");
        SPECIAL_SHIPS.add("ms_boss_charybdis");
        SPECIAL_SHIPS.add("ms_boss_mimir");
        SPECIAL_SHIPS.add("tem_boss_paladin");
        SPECIAL_SHIPS.add("tem_boss_archbishop");
        SPECIAL_SHIPS.add("ssp_boss_phaeton");
        SPECIAL_SHIPS.add("ssp_boss_hammerhead");
        SPECIAL_SHIPS.add("ssp_boss_sunder");
        SPECIAL_SHIPS.add("ssp_boss_tarsus");
        SPECIAL_SHIPS.add("ssp_boss_medusa");
        SPECIAL_SHIPS.add("ssp_boss_falcon");
        SPECIAL_SHIPS.add("ssp_boss_hyperion");
        SPECIAL_SHIPS.add("ssp_boss_paragon");
        SPECIAL_SHIPS.add("ssp_boss_mule");
        SPECIAL_SHIPS.add("ssp_boss_aurora");
        SPECIAL_SHIPS.add("ssp_boss_odyssey");
        SPECIAL_SHIPS.add("ssp_boss_atlas");
        SPECIAL_SHIPS.add("ssp_boss_afflictor");
        SPECIAL_SHIPS.add("ssp_boss_brawler");
        SPECIAL_SHIPS.add("ssp_boss_cerberus");
        SPECIAL_SHIPS.add("ssp_boss_dominator");
        SPECIAL_SHIPS.add("ssp_boss_doom");
        SPECIAL_SHIPS.add("ssp_boss_euryale");
        SPECIAL_SHIPS.add("ssp_boss_lasher_b");
        SPECIAL_SHIPS.add("ssp_boss_lasher_r");
        SPECIAL_SHIPS.add("ssp_boss_onslaught");
        SPECIAL_SHIPS.add("ssp_boss_shade");
        SPECIAL_SHIPS.add("ssp_boss_eagle");
        SPECIAL_SHIPS.add("ssp_boss_beholder");
        SPECIAL_SHIPS.add("ssp_boss_dominator_luddic_path");
        SPECIAL_SHIPS.add("ssp_boss_onslaught_luddic_path");
        SPECIAL_SHIPS.add("ssp_boss_astral");
        SPECIAL_SHIPS.add("ssp_boss_conquest");
        SPECIAL_SHIPS.add("swp_boss_frankenstein");
        SPECIAL_SHIPS.add("ssp_boss_wasp_wing");
        SPECIAL_SHIPS.add("tiandong_boss_wuzhang");
        SPECIAL_SHIPS.add("pack_bulldog_bullseye");
        SPECIAL_SHIPS.add("pack_pitbull_bullseye");
        SPECIAL_SHIPS.add("pack_komondor_bullseye");
        SPECIAL_SHIPS.add("pack_schnauzer_bullseye");
        SPECIAL_SHIPS.add("diableavionics_IBBgulf");
    }

    public static int calculatePowerLevel(CampaignFleetAPI fleet) {
        int power = fleet.getFleetPoints();
        for (FleetMemberAPI member : fleet.getFleetData().getCombatReadyMembersListCopy()) {
            if (member.isCivilian()) {
                power += member.getFleetPointCost() / 2;
            } else {
                power += member.getFleetPointCost();
            }
        }
        int offLvl = 0;
        int cdrLvl = 0;
        boolean commander = false;
        for (OfficerDataAPI officer : fleet.getFleetData().getOfficersCopy()) {
            if (officer.getPerson() == fleet.getCommander()) {
                commander = true;
                cdrLvl = officer.getPerson().getStats().getLevel();
            } else {
                offLvl += officer.getPerson().getStats().getLevel();
            }
        }
        if (!commander) {
            cdrLvl = fleet.getCommanderStats().getLevel();
        }
        power *= Math.sqrt(cdrLvl / 100f + 1f);
        int flatBonus = cdrLvl + offLvl + 10;
        if (power < flatBonus * 2) {
            flatBonus *= power / (float) (flatBonus * 2);
        }
        power += flatBonus;
        return power;
    }

    public static Color colorBlend(Color a, Color b, float amount) {
        float conjAmount = 1f - amount;
        return new Color((int) Math.max(0, Math.min(255, a.getRed() * conjAmount + b.getRed() * amount)),
                         (int) Math.max(0, Math.min(255, a.getGreen() * conjAmount + b.getGreen() * amount)),
                         (int) Math.max(0, Math.min(255, a.getBlue() * conjAmount + b.getBlue() * amount)),
                         (int) Math.max(0, Math.min(255, a.getAlpha() * conjAmount + b.getAlpha() * amount)));
    }

    public static Color colorJitter(Color color, float amount) {
        return new Color(Math.max(0, Math.min(255, color.getRed() + (int) (((float) Math.random() - 0.5f) * amount))),
                         Math.max(0, Math.min(255, color.getGreen() + (int) (((float) Math.random() - 0.5f) * amount))),
                         Math.max(0, Math.min(255, color.getBlue() + (int) (((float) Math.random() - 0.5f) * amount))),
                         color.getAlpha());
    }

    public static float getEconAvailability(String commodityId) {
        float commodityDemand = 0f;
        float commoditySupply = 0f;

        List<MarketAPI> allMarkets = Global.getSector().getEconomy().getMarketsCopy();
        for (MarketAPI market : allMarkets) {
            for (CommodityOnMarketAPI commodity : market.getAllCommodities()) {
                String id = commodity.getId();
                if (!id.contentEquals(commodityId)) {
                    continue;
                }

                commodity.getCommodity().isExotic();
                commodityDemand += commodity.getDemand().getDemandValue();
                commoditySupply += commodity.getSupplyValue() * commodity.getUtilityOnMarket();
            }
        }

        if (commodityDemand > 1f) {
            return commoditySupply / commodityDemand;
        } else {
            return 0f;
        }
    }

    public static int getNonCivilianFleetPoints(FleetDataAPI fleetData) {
        int fp = 0;
        for (FleetMemberAPI member : fleetData.getMembersListCopy()) {
            if (member.isMothballed() || member.isCivilian() || member.getHullSpec().getHints().contains(
                    ShipTypeHints.CIVILIAN)) {
                continue;
            }
            fp += member.getFleetPointCost();
        }
        return Math.max(fp, 1);
    }

    public static boolean isValidRoman(String num) {
        for (int k = 0; k < num.length(); k++) {
            if (num.charAt(k) != 'I' && num.charAt(k) != 'V' && num.charAt(k) != 'X' && num.charAt(k) != 'L' &&
                    num.charAt(k) != 'C' && num.charAt(k) != 'D' &&
                    num.charAt(k) != 'M') {
                return false;
            }
        }
        return true;
    }

    public static float lerp(float x, float y, float alpha) {
        return (1f - alpha) * x + alpha * y;
    }

    /* Pyrolistical on StackExchange */
    public static double roundToSignificantFigures(double num, int n) {
        if (num == 0) {
            return 0;
        }

        final double d = Math.ceil(Math.log10(num < 0 ? -num : num));
        final int power = n - (int) d;

        final double magnitude = Math.pow(10, power);
        final long shifted = Math.round(num * magnitude);
        return shifted / magnitude;
    }

    public static long roundToSignificantFiguresLong(double num, int n) {
        return Math.round(roundToSignificantFigures(num, n));
    }

    public static enum RequiredFaction {

        NONE, JUNK_PIRATES, TIANDONG, SHADOWYARDS, IMPERIUM, TEMPLARS, DIABLE, BLACKROCK, CITADEL, EXIGENCY, MAYORATE,
        SCY, CABAL, ORA;

        public boolean isLoaded() {
            switch (this) {
                case JUNK_PIRATES:
                    return SSPModPlugin.junkPiratesExists;
                case TIANDONG:
                    return SSPModPlugin.tiandongExists;
                case SHADOWYARDS:
                    return SSPModPlugin.shadowyardsExists;
                case IMPERIUM:
                    return SSPModPlugin.imperiumExists;
                case TEMPLARS:
                    return SSPModPlugin.templarsExists;
                case DIABLE:
                    return SSPModPlugin.diableExists;
                case BLACKROCK:
                    return SSPModPlugin.blackrockExists;
                case CITADEL:
                    return SSPModPlugin.citadelExists;
                case EXIGENCY:
                    return SSPModPlugin.exigencyExists;
                case MAYORATE:
                    return SSPModPlugin.mayorateExists;
                case SCY:
                    return SSPModPlugin.scyExists;
                case CABAL:
                    return SSPModPlugin.hasUnderworld;
                case ORA:
                    return SSPModPlugin.oraExists;
                case NONE:
                default:
                    return true;
            }
        }
    }
}
